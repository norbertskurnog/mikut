package pl.edu.agh.student.mikut.billing_extender;

/**
 * Created by pawnow on 2014-11-22.
 */
public class BillingElement {

    private String phoneNumber;
    private int callTime;
    private int smsAmount;

    public BillingElement(String phoneNumber, int callTime, int smsAmount){
        this.phoneNumber = phoneNumber;
        this.callTime = callTime;
        this.smsAmount = smsAmount;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getCallTime() {
        return callTime;
    }

    public void setCallTime(int callTime) {
        this.callTime = callTime;
    }

    public int getSmsAmount() {
        return smsAmount;
    }

    public void setSmsAmount(int smsAmount) {
        this.smsAmount = smsAmount;
    }

}
