package pl.edu.agh.student.mikut.billing_extender;

/**
 * Created by pawnow on 2014-11-22.
 */
public class BillingElementExtended extends BillingElement{

    private Operator operatorName;

    public BillingElementExtended(String phoneNumber, int callTime, int smsAmount){
        super( phoneNumber, callTime, smsAmount);
    }

    public BillingElementExtended(String phoneNumber, int callTime, int smsAmount, Operator operator){
        super( phoneNumber, callTime, smsAmount);
        this.operatorName = operator;
    }

    public BillingElementExtended(BillingElement billing, Operator operator){
        super(billing.getPhoneNumber(), billing.getCallTime(), billing.getSmsAmount());
        this.operatorName = operator;
    }

    public Operator getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(Operator operatorName) {
        this.operatorName = operatorName;
    }

}
