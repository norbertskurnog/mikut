package pl.edu.agh.student.mikut.billing_extender;

import java.io.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by pawnow on 2014-11-22.
 */
public class FileOperator {

    public boolean saveFile(Collection<BillingElementExtended> billing){
        Map<String, Operator> fileOperators = readFile();
        File operatorCache = new File("operatorCache.txt");
        if(!operatorCache.exists()){
            try {
                operatorCache.createNewFile();
            } catch (IOException e) {
                return false;
            }
        }
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter("operatorCache.txt", true));
            out.write(Long.toString(System.currentTimeMillis())+"\n");
            out.newLine();
            for(BillingElementExtended elem: billing){
                if(elem.getOperatorName() != Operator.Unknown){
                    if(!fileOperators.containsKey(elem.getPhoneNumber())){
                        out.write(elem.getPhoneNumber()+","+elem.getOperatorName().toString()+"\n");
                        out.newLine();
                    }
                }
            }
            out.close();
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    public Map<String, Operator> readFile(){
        File operatorCache = new File("operatorCache.txt");
        HashMap<String, Operator> map = new HashMap<>();
        if(!operatorCache.exists()){
            return map;
        }
        deleteUnactualData();
        try{
            BufferedReader reader = new BufferedReader(new FileReader("operatorCache.txt"));
            String textLine;
            while((textLine = reader.readLine()) != null){
                String[] splitted = textLine.split(",");
                if(splitted.length == 2){
                    String number = splitted[0];
                    Operator op = Operator.valueOf(splitted[1]);
                    map.put(number, op);
                }
            }
            reader.close();
        } catch (FileNotFoundException e) {
            return map;
        } catch (IOException e) {
            return map;
        }
        return map;
    }

    private boolean deleteUnactualData(){
        File cacheFile = new File("operatorCache.txt");
        File newCacheFile = new File("newOperatorCache.txt");
        if(!cacheFile.exists()){
            return true;
        }
        try{
            BufferedReader reader = new BufferedReader(new FileReader("operatorCache.txt"));
            BufferedWriter writer = new BufferedWriter(new FileWriter("newOperatorCache.txt", true));
            String textLine;
            Long date = 0L;
            while((textLine = reader.readLine()) != null){
                String[] splitted = textLine.split(",");
                if(splitted.length == 1 && !textLine.equals("")){
                    date = Long.parseLong(textLine.split(",")[0]);
                    if(System.currentTimeMillis()-date <= 86400000){
                        writer.write(textLine);
                        writer.newLine();
                    }
                }
                else if(splitted.length == 2 && System.currentTimeMillis()-date <= 86400000){
                    writer.write(textLine);
                    writer.newLine();
                }
            }
            reader.close();
            writer.close();
        } catch (FileNotFoundException e) {
            return false;
        } catch (IOException e) {
            return false;
        }
        cacheFile.delete();
        return newCacheFile.renameTo(cacheFile);
    }

}
