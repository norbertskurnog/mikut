package pl.edu.agh.student.mikut.billing_extender;

/**
 * Created by pawnow on 2014-11-22.
 */
public enum Operator {
    Play,
    Plus,
    Orange,
    TMobile,
    Unknown
}
