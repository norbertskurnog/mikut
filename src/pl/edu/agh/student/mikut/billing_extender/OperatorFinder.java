package pl.edu.agh.student.mikut.billing_extender;

import pl.edu.agh.student.mikut.comparator.OfferComparator;
import pl.edu.agh.student.mikut.operator_finders.OperatorAssign;
import pl.edu.agh.student.mikut.operator_finders.OperatorAssignSMSPriv;
import pl.edu.agh.student.mikut.operator_finders.SiteConnector;

import javax.swing.*;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by pawnow on 2014-11-22.
 */
public class OperatorFinder {

    // TODO: usunac jedno z tych dwoch
    public void setBillingOperator(Collection<BillingElement> billing, List<OperatorAssign> sources){
        Collection<BillingElementExtended> operatorsBilling = findOperators(billing, sources, new SiteConnector());
        invokeOfferComparator(operatorsBilling);
    }

    public void setBillingOperator(OfferComparator offerComparator, Collection<BillingElement> billing, List<OperatorAssign> sources){
        Collection<BillingElementExtended> operatorsBilling = findOperators(billing, sources, new SiteConnector());
        invokeOfferComparator(offerComparator, operatorsBilling);
    }

    private Collection<BillingElementExtended> findOperators(Collection<BillingElement> billing,
                                                             List<OperatorAssign> sources, SiteConnector connector){

        Collection<BillingElementExtended> operatorsBilling = new LinkedList<BillingElementExtended>();
        Operator operator;
        int currentProgress = 0;
        JProgressBar progressBar = new JProgressBar(0, billing.size());
        progressBar.setValue(currentProgress);
        progressBar.setStringPainted(true);
        JFrame frame = showProgressBar(progressBar);
        FileOperator file = new FileOperator();
        Map<String, Operator> fileOperators = file.readFile();
        for(BillingElement element: billing){
            operator = Operator.Unknown;
            String number = element.getPhoneNumber();
            if(fileOperators.containsKey(number)){
                operator = fileOperators.get(number);
            }
            else{
                for(int i = 0; i < sources.size(); i++){
                    OperatorAssign operatorAssigner = sources.get(i);
                    if(operator != Operator.Unknown)
                        break;
                    operator = operatorAssigner.getOperator(number, connector);
                }
            }
            operatorsBilling.add(new BillingElementExtended(element, operator));
            currentProgress++;
            progressBar.setValue(currentProgress);
        }
        frame.setVisible(false);
        frame.dispose();
        file.saveFile(operatorsBilling);
        return operatorsBilling;
    }

    private JFrame showProgressBar(JProgressBar progressBar){
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel panel = new JPanel();
        panel.add(progressBar);
        frame.setContentPane(panel);
        frame.pack();
        frame.setVisible(true);
        return frame;
    }

    // TODO: usunac jedno z tych dwoch
    private void invokeOfferComparator(Collection<BillingElementExtended> billing){
        OfferComparator modul3 = new OfferComparator();
        modul3.setBilling((List<BillingElementExtended>) billing);
    }

    private void invokeOfferComparator(OfferComparator offerComparator, Collection<BillingElementExtended> billing){
        offerComparator.setBilling((List<BillingElementExtended>) billing);
    }

}
