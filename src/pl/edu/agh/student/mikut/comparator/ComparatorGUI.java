package pl.edu.agh.student.mikut.comparator;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.List;
import javax.swing.JFileChooser;

import pl.edu.agh.student.mikut.billing_extender.BillingElementExtended;
import pl.edu.agh.student.mikut.parser.GUI;

public class ComparatorGUI extends JFrame {
    private DefaultTableModel billingsViewTableModel;
    private DefaultTableModel offersViewTableModel;
    private JTable offerDetailsTable;
    private JTable offersViewTable;

    private DefaultTableModel offerDetailsTableModel;

    private JButton compareButton;

    private OfferComparator comparator;

    public ComparatorGUI(final OfferComparator comparator){
        this.comparator = comparator;

        setLayout(new BorderLayout());

        JPanel dataViewPanel = new JPanel();
        JPanel controlButtonsPanel = new JPanel();

        dataViewPanel.setLayout(new BoxLayout(dataViewPanel, BoxLayout.LINE_AXIS));
        this.add(dataViewPanel, BorderLayout.CENTER);
        this.add(controlButtonsPanel, BorderLayout.PAGE_END);

        // billings view panel
        JPanel billingsViewPanel = new JPanel();
        billingsViewPanel.setLayout(new BorderLayout());

        // table
        billingsViewTableModel = new DefaultTableModel();
        billingsViewTableModel.setColumnIdentifiers(new String[]{"Operator", "Number", "Call time", "SMS"});
        JTable billingsViewTable = new JTable(billingsViewTableModel);

        // billing view control buttons
        JPanel billingsViewButtonsPanel = new JPanel();
        billingsViewButtonsPanel.setLayout(new FlowLayout());

        JButton billingsViewAddButton = new JButton("Add");
        billingsViewAddButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GUI parserGUI = new GUI(comparator);

                JFileChooser fileChooser = new JFileChooser();
                int retVal = fileChooser.showOpenDialog(null);
                if(retVal == JFileChooser.APPROVE_OPTION){
                    File billingFile = fileChooser.getSelectedFile();
                    parserGUI.run(billingFile);
                }
            }
        });

        JButton billingsViewRemoveButton = new JButton("Remove");
        billingsViewRemoveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                comparator.billingsData.clear();
                billingsViewTableModel.setRowCount(0);
                billingsViewTableModel.fireTableDataChanged();
                compareButton.setEnabled(false);
            }
        });

        billingsViewButtonsPanel.add(billingsViewAddButton);
        billingsViewButtonsPanel.add(billingsViewRemoveButton);

        billingsViewPanel.add(new JLabel("Billings"), BorderLayout.PAGE_START);
        billingsViewPanel.add(new JScrollPane(billingsViewTable), BorderLayout.CENTER);
        billingsViewPanel.add(billingsViewButtonsPanel, BorderLayout.PAGE_END);


        // offers view panel
        JPanel offersViewPanel = new JPanel();
        offersViewPanel.setLayout(new BorderLayout());

        // table
        offersViewTableModel = new DefaultTableModel();
        offersViewTableModel.setColumnIdentifiers(new Object[]{"Offer name", "Operator"});
        offersViewTable = new JTable(offersViewTableModel);
        offersViewTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                int rowIndex = offersViewTable.getSelectedRow();
                if(rowIndex > -1)
                    displayOfferDetails(comparator.offersData.get(rowIndex));
            }
        });

        // details table
        offerDetailsTableModel = new DefaultTableModel();
        offerDetailsTableModel.setColumnIdentifiers(new Object[]{"Service", "Unit", "Value", "Operator"});
        offerDetailsTable = new JTable(offerDetailsTableModel);

        // offer view control buttons
        JPanel offersViewButtonsPanel = new JPanel();
        offersViewButtonsPanel.setLayout(new FlowLayout());

        JButton offersViewAddButton = new JButton("Add");
        offersViewAddButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new OfferAddMenu(comparator, null);
            }
        });

        JButton offersViewModifyButton = new JButton("Modify");
        offersViewModifyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedIdx = offersViewTable.getSelectedRow();
                if (selectedIdx > -1) {
                    Offer offer = comparator.offersData.get(selectedIdx);

                    new OfferAddMenu(comparator, offer);
                }
            }
        });

        JButton offersViewRemoveButton = new JButton("Remove");
        offersViewRemoveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedIdx = offersViewTable.getSelectedRow();
                if (selectedIdx > -1) {
                    Offer offer = comparator.offersData.get(selectedIdx);

                    DBConnector connector = new DBConnector();
                    connector.removeOfferFromDB(offer);
                    comparator.offersData.clear();

                    // reload offers to maintain order
                    comparator.offersData = connector.getOfferData();

                    offersViewTableModel.setRowCount(0);
                    displayOffersData();
                }
            }
        });

        offersViewButtonsPanel.add(offersViewAddButton);
        offersViewButtonsPanel.add(offersViewModifyButton);
        offersViewButtonsPanel.add(offersViewRemoveButton);

        offersViewPanel.add(new JLabel("Offers"), BorderLayout.PAGE_START);

        JPanel offerViewTables = new JPanel();
        offerViewTables.setLayout(new BoxLayout(offerViewTables, BoxLayout.PAGE_AXIS));
        offerViewTables.add(new JScrollPane(offersViewTable));
        offerViewTables.add(new JScrollPane(offerDetailsTable));
        offersViewPanel.add(offerViewTables, BorderLayout.CENTER);

        offersViewPanel.add(offersViewButtonsPanel, BorderLayout.PAGE_END);

        dataViewPanel.add(billingsViewPanel);
        dataViewPanel.add(offersViewPanel);

        // module control buttons
        compareButton = new JButton("Compare offers");
        compareButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                List<Offer> rank = comparator.compareOffers();
                new ResultPresentation(rank);
            }
        });
        compareButton.setEnabled(false);
        controlButtonsPanel.add(compareButton);

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
        this.pack();
    }

    public void displayBillingData(){

        for(BillingElementExtended billingElement: comparator.billingsData){
            Object tableRow[] = new Object[4];

            tableRow[0] = billingElement.getOperatorName();
            tableRow[1] = billingElement.getPhoneNumber();
            tableRow[2] = billingElement.getCallTime();
            tableRow[3] = billingElement.getSmsAmount();

            billingsViewTableModel.addRow(tableRow);
        }

        billingsViewTableModel.fireTableDataChanged();

        if (!comparator.offersData.isEmpty()){
            this.compareButton.setEnabled(true);
        }
    }

    public void displayOffersData(){
        // clear table
        offersViewTableModel.setRowCount(0);
        offerDetailsTableModel.setRowCount(0);

        for(Offer offer: comparator.offersData){
            Object tableRow[] = new Object[2];

            tableRow[0] = offer.getOfferProvider();
            tableRow[1] = offer.getOfferName();

            offersViewTableModel.addRow(tableRow);
        }

        offersViewTableModel.fireTableDataChanged();

        if (!comparator.billingsData.isEmpty()){
            this.compareButton.setEnabled(true);
        }
    }

    private void displayOfferDetails(Offer offer){

        Object tableRow[] = new Object[4];

        this.offerDetailsTableModel.setRowCount(0);
        this.offerDetailsTable.revalidate();

        List<OfferService> services = offer.getServices();


        for(OfferService service: services){
            tableRow[1] = service.getUnit();
            tableRow[2] = service.getPrice();
            tableRow[3] = service.getDestOperator().toString();

            if(service.getType().equals(ServiceType.Call)){
                tableRow[0] = "Call";
            } else if (service.getType().equals(ServiceType.SMS)) {
                tableRow[0] = "SMS";
            } else if (service.getType().equals(ServiceType.SMS_pack)) {
                tableRow[0] = "SMS Pack";
            } else if (service.getType().equals(ServiceType.Internet)) {
                tableRow[0] = "Internet";
                tableRow[3] = "";
            } else if (service.getType().equals(ServiceType.MMS)){
                tableRow[0] = "MMS";
            }

            offerDetailsTableModel.addRow(tableRow);
        }

        offerDetailsTableModel.fireTableDataChanged();
    }
}
