package pl.edu.agh.student.mikut.comparator;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import org.hibernate.cfg.Configuration;

import java.io.File;
import java.util.List;


// TODO: get this opening and closing right...
// TODO: SQLite?
public class DBConnector {

    private Session currentSession;
    private SessionFactory sessions;
    private Configuration configuration;

    public DBConnector() {
        configuration = new Configuration();
        configuration.configure(new File("hibernate.cfg.xml"));

//        System.out.println(System.getProperty("user.dir"));
    }

    public List<Offer> getOfferData(){
        // TODO: check whatever needs to be checked...
        sessions = configuration.buildSessionFactory();
        currentSession = sessions.openSession();

        List<Offer> offersList = currentSession.createCriteria(Offer.class).list();
        for (Offer offer: offersList){
            Hibernate.initialize(offer);
        }

        currentSession.close();
        sessions.close();

        return offersList;
    }

    public void saveOfferInDB(Offer offer) {
        sessions = configuration.buildSessionFactory();
        currentSession = sessions.openSession();

        Transaction transaction = currentSession.beginTransaction();
        currentSession.persist(offer);
        transaction.commit();

        currentSession.close();
        sessions.close();
    }

    public void removeOfferFromDB(Offer offer){
        sessions = configuration.buildSessionFactory();
        currentSession = sessions.openSession();

        Transaction transaction = currentSession.beginTransaction();
        currentSession.delete(offer);
        transaction.commit();

        currentSession.close();
        sessions.close();
    }

    public void closeConnection() {
        currentSession.close();
        sessions.close();
    }
}
