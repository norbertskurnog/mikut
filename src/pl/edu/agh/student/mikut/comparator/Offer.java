package pl.edu.agh.student.mikut.comparator;

import pl.edu.agh.student.mikut.billing_extender.Operator;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="Offers")
public class Offer implements Comparable<Offer>{

    @Id
    @GeneratedValue
    private int id;

    @Column(name="offer_name")
    private String offerName;

    @Enumerated (EnumType.STRING)
    @Column(name="provider")
    private Operator offerProvider;

    @OneToMany (mappedBy = "offerId", cascade=CascadeType.ALL, fetch=FetchType.EAGER)
    private List<OfferService> services;

    @Transient
    public double offerCost;

    public Offer(){
    }

    public Offer(String offerName, Operator offerProvider, List<OfferService> services){
        this.offerName = offerName;
        this.offerProvider = offerProvider;

        this.services = services;
    }

    @Override
    public int compareTo(Offer other){
        return (new Double(this.offerCost)).compareTo(other.offerCost);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOfferName() {
        return offerName;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    public Operator getOfferProvider() {
        return offerProvider;
    }

    public void setOfferProvider(Operator offerProvider) {
        this.offerProvider = offerProvider;
    }

    public List<OfferService> getServices() {
        return services;
    }

    public void setServices(List<OfferService> services) {
        this.services = services;
    }
}
