package pl.edu.agh.student.mikut.comparator;

import pl.edu.agh.student.mikut.billing_extender.Operator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;
import java.util.List;

// TODO: check doubled services
// TODO: add "all" operator type
// TODO: modify/remove offer
public class OfferAddMenu extends JFrame {

    private JPanel offerServicesPanel;

    private Offer createdOffer;

    private JFrame window;

    public OfferAddMenu(final OfferComparator offerComparator, final Offer existingOffer){
        window = this;
        setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));

        // name of added offer
        JPanel basicInfoPanel = new JPanel();
        basicInfoPanel.setLayout(new BoxLayout(basicInfoPanel, BoxLayout.LINE_AXIS));

        final JTextField offerName = new JTextField("New offer");
        basicInfoPanel.add(offerName);

        final JComboBox operatorChooserCB = new JComboBox<>(Operator.values());
        basicInfoPanel.add(operatorChooserCB);

        add(basicInfoPanel);

        offerServicesPanel = new JPanel();
        offerServicesPanel.setLayout(new BoxLayout(offerServicesPanel, BoxLayout.PAGE_AXIS));
        if(existingOffer == null)
            offerServicesPanel.add(new ServiceGUIRepresentation(null));
        else {
            offerName.setText(existingOffer.getOfferName());
            operatorChooserCB.setSelectedItem(existingOffer.getOfferProvider());
            for(OfferService service: existingOffer.getServices()){
                offerServicesPanel.add(new ServiceGUIRepresentation(service));
            }
        }

        add(offerServicesPanel);

        // panel with control buttons
        JButton addServiceButton = new JButton("Add service");
        JButton doneButton = new JButton("Done");
        JButton cancelButton = new JButton("Cancel");

        JPanel upperButtonsPanel = new JPanel();
        upperButtonsPanel.add(addServiceButton);

        JPanel lowerButtonsPanel = new JPanel();
        lowerButtonsPanel.add(doneButton);
        lowerButtonsPanel.add(cancelButton);

        JPanel buttonsPanel = new JPanel();
        buttonsPanel.setLayout(new GridLayout(2, 1));
        buttonsPanel.add(upperButtonsPanel);
        buttonsPanel.add(lowerButtonsPanel);

        this.add(buttonsPanel);

        this.pack();
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        this.setVisible(true);

        addServiceButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e){
                offerServicesPanel.add(new ServiceGUIRepresentation(null));
                offerServicesPanel.revalidate();
                offerServicesPanel.repaint();
                window.pack();
            }
        });

        doneButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                List<OfferService> services = new LinkedList<OfferService>();
                Operator chosenOperator = Operator.valueOf(operatorChooserCB.getSelectedItem().toString());
                createdOffer = new Offer(offerName.getText(), chosenOperator , services);

                Object offerDataPanels[] = offerServicesPanel.getComponents();
                for(Object servicePanel: offerDataPanels) {
                    ServiceType type = ServiceType.valueOf(((JComboBox<String>) (((JPanel) servicePanel).getComponent(0))).getSelectedItem().toString());
                    float value = new Float(((JSpinner) (((JPanel) servicePanel).getComponent(1))).getValue().toString());
                    int unit = new Integer(((JComboBox<String>) (((JPanel) servicePanel).getComponent(2))).getSelectedItem().toString());
                    Operator destOperator = Operator.valueOf(((JComboBox<String>) (((JPanel) servicePanel).getComponent(3))).getSelectedItem().toString());

                    OfferService service = new OfferService(type, unit, value, destOperator, createdOffer);
                    services.add(service);
                }

                if (existingOffer != null){
                    (new DBConnector()).removeOfferFromDB(existingOffer);
                    offerComparator.offersData.clear();

                    // reload offers to maintain order
                    offerComparator.offersData = (new DBConnector()).getOfferData();
                }
                offerComparator.addOffer(createdOffer);
                window.dispose();
            }
        });
    }
}
