package pl.edu.agh.student.mikut.comparator;

import org.hibernate.Hibernate;
import pl.edu.agh.student.mikut.billing_extender.BillingElementExtended;
import pl.edu.agh.student.mikut.billing_extender.Operator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class OfferComparator {

    private ComparatorGUI moduleGUI;
    private DBConnector connector = new DBConnector();

    public List<BillingElementExtended> billingsData = new ArrayList<>();
    public List<Offer> offersData = new ArrayList<>();

    public OfferComparator(){
        // open database connection
        // TODO: catch exception or anything in case of problems connecting to database
        offersData = connector.getOfferData();

        moduleGUI = new ComparatorGUI(this);
        moduleGUI.displayOffersData();
    }

    public void setBilling(List<BillingElementExtended> billingData){
        this.billingsData = billingData;

        moduleGUI.displayBillingData();
    }

    public void addOffer(Offer offer){
        connector.saveOfferInDB(offer);
        offersData.add(offer);

        moduleGUI.displayOffersData();
    }

    /**
     * Ranking algorithm, operators and units are ignored for now.
     * If offer has multiple services of same type, last one in the list will be used.
     * SMS packets are not supported yet.
     *
     * @return sorted ranking
     */
    // TODO: more detailed results, support for ^
    // TODO: if service is not present in offer...
    public List<Offer> compareOffers(){
        List<Offer> rank = new LinkedList<Offer>();

        // calculate overall cost of billing
        for (Offer offer: this.offersData){
            float offerCost = 0;

            double callCost = -1, smsCost = -1, internetCost = -1;

            for (BillingElementExtended billingElement: this.billingsData){
                Operator destOperator = billingElement.getOperatorName();

                if(billingElement.getPhoneNumber().equals("Internet")){
                    for (OfferService service: offer.getServices()){
                        if(service.getType().equals(ServiceType.Internet)){
                            int priceUnits = (int)Math.ceil(billingElement.getCallTime()/(double)service.getUnit());
                            internetCost += service.getPrice() * priceUnits;
                            break;
                        }
                    }
                    continue;
                }

                if(!destOperator.equals(Operator.Unknown)){
                    double smsSingleCost = -1;
                    double smsPackedCost = -1;
                    for (OfferService service: offer.getServices()){
                        if(service.getDestOperator().equals(destOperator)){
                            switch (service.getType()){
                                case Call: {
                                    int priceUnits = (int)Math.ceil(billingElement.getCallTime()/(double)service.getUnit());
                                    callCost += service.getPrice() * priceUnits;
                                    break;
                                }
                                case SMS: {
                                    // calculate cost without any packs
                                    smsSingleCost = service.getPrice() * billingElement.getSmsAmount();
                                    break;
                                }
                                case SMS_pack: {
                                    // TODO: mixed costs, not all messages in packs...
                                    smsPackedCost = service.getPrice() * Math.ceil(billingElement.getSmsAmount() / (double)service.getUnit());
                                    break;
                                }
                            }
                        }
                    }
                    if (smsPackedCost == -1) smsCost = smsSingleCost;
                    else if (smsSingleCost == -1) smsCost = smsPackedCost;
                    else{
                        smsCost = smsPackedCost < smsSingleCost ? smsPackedCost : smsSingleCost;
                    }
                } else {
                    double sumCallPrice = 0, noCallServices = 0, sumSmsPrice = 0, noSmsServices = 0;
                    for(OfferService service: offer.getServices()){
                        switch (service.getType()) {
                            case Call: {
                                sumCallPrice += service.getPrice();
                                noCallServices++;
                                break;
                            }
                            case SMS: {
                                sumSmsPrice += service.getPrice();
                                noSmsServices++;
                            }
                        }
                    }

                    /**
                     * If destination operator is unknown, assume that:
                     * price of service is average of prices of the same service in this offer
                     * pricing unit is: 60s for call, 1 for sms
                     * no sms pack is used
                     */
                    if(noCallServices != 0) {
                        callCost += (sumCallPrice / noCallServices) * (int)Math.ceil(billingElement.getCallTime()/60);
                    }
                    if(noSmsServices != 0){
                        smsCost += (sumSmsPrice / noSmsServices) * billingElement.getSmsAmount();
                    }
                }

                offerCost += callCost + smsCost + internetCost;
            }

            offer.offerCost = offerCost;
            rank.add(offer);
        }

        Collections.sort(rank);

        return rank;
    }
}
