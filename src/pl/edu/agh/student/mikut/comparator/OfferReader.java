package pl.edu.agh.student.mikut.comparator;

import java.util.List;

// TODO: class is unnecessary and can be removed, do it...
public class OfferReader {

    private OfferAddMenu offerReaderGUI;
    private DBConnector dbConnector;

    public OfferReader(){
    }

    public List<Offer> getOffersFromDB(){
        return this.dbConnector.getOfferData();
    }

    public List<Offer> getOfferFromUser(){
        offerReaderGUI.setVisible(true);
        return this.dbConnector.getOfferData();
    }

    public void addOffer(Offer offer){
        dbConnector.saveOfferInDB(offer);
    }
}
