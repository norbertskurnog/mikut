package pl.edu.agh.student.mikut.comparator;

import pl.edu.agh.student.mikut.billing_extender.Operator;

import javax.persistence.*;

@Entity
@Table(name="OfferDetails")
public class OfferService {

    // TODO: make primary key composite
    @Id
    @GeneratedValue
    private int serviceId;

    @ManyToOne
    @JoinColumn(name="offerId")
    private Offer offerId;

    @Enumerated(EnumType.STRING)
    @Column(name="type")
    private ServiceType type;

    @Enumerated(EnumType.STRING)
    @Column(name="destOperator")
    private Operator destOperator;

    @Column(name="unit")
    private int unit;

    @Column(name="price")
    private double price;

    public OfferService(){
    }

    public OfferService(ServiceType type, int unit, double price, Operator destOperator, Offer offer){
        this.type = type;
        this.unit = unit;
        this.price = price;
        this.destOperator = destOperator;
        this.offerId = offer;
    }

    public int getServiceId() {
        return serviceId;
    }

    public void setServiceId(int serviceId) {
        this.serviceId = serviceId;
    }

    public Offer getOfferId() {
        return offerId;
    }

    public void setOfferId(Offer offerId) {
        this.offerId = offerId;
    }

    public ServiceType getType() {
        return type;
    }

    public void setType(ServiceType type) {
        this.type = type;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Operator getDestOperator() {return this.destOperator;}

    public void setDestOperator(Operator destOperator) {this.destOperator = destOperator;}
}
