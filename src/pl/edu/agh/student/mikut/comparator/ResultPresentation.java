package pl.edu.agh.student.mikut.comparator;

import com.sun.deploy.panel.JreTableModel;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.List;

public class ResultPresentation extends JFrame {

    DefaultTableModel offerDetailsTableModel = new DefaultTableModel();
    JTable offerDetailsTable = new JTable(offerDetailsTableModel);

    public ResultPresentation(final List<Offer> rank){

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(3, 1));

        mainPanel.add(new JLabel("Results"));

        // table with results
        DefaultTableModel resultsTableModel = new DefaultTableModel();
        resultsTableModel.setColumnIdentifiers(new Object[]{"Operator", "Name", "Cost"});
        final JTable resultsTable = new JTable(resultsTableModel);

        resultsTable.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                int rowIndex = resultsTable.getSelectedRow();
                displayOfferDetails(rank.get(rowIndex));
            }
        });


        for(Offer offer: rank){
            Object tableRow[] = new Object[3];
            tableRow[0] = offer.getOfferProvider();
            tableRow[1] = offer.getOfferName();
            tableRow[2] = offer.offerCost;

            resultsTableModel.addRow(tableRow);
        }
        resultsTableModel.fireTableDataChanged();

        mainPanel.add(new JScrollPane(resultsTable), BorderLayout.CENTER);

        // details table
        offerDetailsTableModel.setColumnIdentifiers(new String[]{"Service", "Unit", "Price"});
        mainPanel.add(new JScrollPane(offerDetailsTable), BorderLayout.CENTER);

        add(mainPanel);

        this.pack();
        this.setVisible(true);
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    }

    private void displayOfferDetails(Offer offer){

        Object tableRow[] = new Object[3];

        this.offerDetailsTableModel.setRowCount(0);
        this.offerDetailsTable.revalidate();

        List<OfferService> services = offer.getServices();

        for(OfferService service: services){
            if(service.getType().equals(ServiceType.Call)){
                tableRow[0] = "Call";
            } else if (service.getType().equals(ServiceType.SMS)) {
                tableRow[0] = "SMS";
            } else if (service.getType().equals(ServiceType.SMS_pack)) {
                tableRow[0] = "SMS Pack";
            } else if (service.getType().equals(ServiceType.Internet)) {
                tableRow[0] = "Internet";
            } else if (service.getType().equals(ServiceType.MMS)){
                tableRow[0] = "MMS";
            }

            tableRow[2] = service.getPrice();
            tableRow[1] = service.getUnit();
            offerDetailsTableModel.addRow(tableRow);
        }

        offerDetailsTableModel.fireTableDataChanged();
    }
}
