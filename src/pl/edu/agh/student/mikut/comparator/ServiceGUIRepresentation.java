package pl.edu.agh.student.mikut.comparator;

import pl.edu.agh.student.mikut.billing_extender.Operator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

// TODO: units with units, not pointless numbers
// TODO: editable units
// TODO: label for SMS_pack
// TODO: 'all/none' operator
public class ServiceGUIRepresentation extends JPanel{

    private JComboBox<String> serviceUnitCombo;
    String[] internet_serviceUnitComboModel = {"10", "100", "1000"};
    String[] call_serviceUnitComboModel = {"1", "60", "3600"};
    String[] sms_serviceUnitComboModel = {"1"};
    String[] smspack_serviceUnitComboModel = {"10", "100", "500", "1000000"};

    private JComboBox destOperator = new JComboBox<>(Operator.values());


    public ServiceGUIRepresentation(OfferService service){
        super();
        setLayout(new GridLayout(1, 5));

        final JComboBox serviceTypeCombo = new JComboBox<>(ServiceType.values());
        serviceUnitCombo = new JComboBox<>();
        serviceUnitCombo.setEnabled(true);
        serviceUnitCombo.setModel(new DefaultComboBoxModel<>(call_serviceUnitComboModel));

        serviceTypeCombo.addItemListener(new ItemListener() {
             @Override
             public void itemStateChanged(ItemEvent e) {
                 if (e.getStateChange() == ItemEvent.SELECTED) {
                     if (e.getItem().equals(ServiceType.Call)){
                         serviceUnitCombo.setModel(new DefaultComboBoxModel<>(call_serviceUnitComboModel));
                         serviceUnitCombo.setEnabled(true);
                         destOperator.setEnabled(true);
                     }
                     else if (e.getItem().equals(ServiceType.SMS)){
                         serviceUnitCombo.setModel(new DefaultComboBoxModel<>(sms_serviceUnitComboModel));
                         serviceUnitCombo.setEnabled(false);
                         destOperator.setEnabled(true);
                     }
                     else if (e.getItem().equals(ServiceType.Internet)){
                         serviceUnitCombo.setModel(new DefaultComboBoxModel<>(internet_serviceUnitComboModel));
                         serviceUnitCombo.setEnabled(true);
                         destOperator.setEnabled(false);
                     }
                     else if (e.getItem().equals(ServiceType.SMS_pack)){
                         serviceUnitCombo.setModel(new DefaultComboBoxModel<>(smspack_serviceUnitComboModel));
                         serviceUnitCombo.setEnabled(true);
                         destOperator.setEnabled(true);
                     }
                     else if (e.getItem().equals(ServiceType.MMS)){
                         serviceUnitCombo.setModel(new DefaultComboBoxModel<>(sms_serviceUnitComboModel));
                         serviceUnitCombo.setEnabled(false);
                         destOperator.setEnabled(true);
                     }
                 }
             }
         });
        serviceTypeCombo.setSelectedItem(serviceTypeCombo.getItemAt(1));

        JSpinner serviceCostSpinner = new JSpinner();

        JButton serviceRemoveButton = new JButton("X");
        serviceRemoveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JPanel currentPanel = (JPanel)((JButton)actionEvent.getSource()).getParent();
                JPanel offerServicesPanel = (JPanel)currentPanel.getParent();
                offerServicesPanel.remove(currentPanel);
                offerServicesPanel.revalidate();
                offerServicesPanel.repaint();
            }
        });

        if(service != null){
            serviceTypeCombo.setSelectedItem(service.getType());
            serviceCostSpinner.setValue(service.getPrice());
            serviceUnitCombo.setSelectedItem(service.getUnit());
            destOperator.setSelectedItem(service.getDestOperator());
        }

        add(serviceTypeCombo);
        add(serviceCostSpinner);
        add(serviceUnitCombo);
        add(destOperator);
        add(serviceRemoveButton);

        setVisible(true);
    }
}
