package pl.edu.agh.student.mikut.comparator;

public enum ServiceType {
    Call,
    SMS,
    SMS_pack,
    MMS,
    Internet
}
