package pl.edu.agh.student.mikut.operator_finders;

import pl.edu.agh.student.mikut.billing_extender.Operator;

/**
 * Created by pawnow on 2014-11-22.
 */
public interface OperatorAssign {

    public Operator getOperator(String phoneNumber, SiteConnector connector);

    public Operator convertStringToOperator(String operator);

}
