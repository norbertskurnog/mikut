package pl.edu.agh.student.mikut.operator_finders;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import pl.edu.agh.student.mikut.billing_extender.Operator;

/**
 * Created by pawnow on 2014-11-22.
 */
public class OperatorAssignSMSPriv implements OperatorAssign {

    private WebDriver driver = new HtmlUnitDriver();

    /**
     * Returns an operator name. The parameter number should be 9 characters long
     * and contains numbers. The method connects to the http://sms.priv.pl/analiza-numerow/ site.
     * Then it fills in the form and gets operator name reloaded page.
     * Method searches the page by parsing it. If site change method might stop working.
     * @param number - 9 character long representation of phone number
     * @return operator name of the number or null if it couldn't be found
     */
    @Override
    public Operator getOperator(String number, SiteConnector connector) {
        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(java.util.logging.Level.OFF);
        java.util.logging.Logger.getLogger("org.apache.http").setLevel(java.util.logging.Level.OFF);

        if(number.length() != 9) {
            return Operator.Unknown;
        }
        try {
            driver = connector.getPage(driver, "http://sms.priv.pl/analiza-numerow/");
            WebElement number_form = driver.findElement(By.name("numer2"));
            WebElement button = driver.findElement(By.xpath("/html/body/table/tbody/tr[7]/td[1]/form/input[2]"));
            number_form.sendKeys(number);
            driver = connector.getFormResult(driver, button);
            String operator = driver.findElement(By.xpath("/html/body/table/tbody/tr[7]/td[2]/b[3]")).getText();
            return convertStringToOperator(operator);
        }
        catch(NoSuchElementException e){
            return Operator.Unknown;
        }
    }

    @Override
    public Operator convertStringToOperator(String operator){
        switch(operator){
            case "Heyah": return Operator.TMobile;
            case "PTC": return Operator.TMobile;
            case "PTC / T-Mobile": return Operator.TMobile;
            case "Plus" : return Operator.Plus;
            case "Plus / Simplus / 36.6 / mBank / Mova / FmMobile / FreeM": return Operator.Plus;
            case "Orange": return Operator.Orange;
            case "P4 - Play": return Operator.Play;
            default: System.out.println(operator); return Operator.Unknown;
        }
    }
}
