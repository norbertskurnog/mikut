package pl.edu.agh.student.mikut.operator_finders;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import pl.edu.agh.student.mikut.billing_extender.Operator;

/**
 * Created by pawnow on 2015-01-16.
 */
public class OperatorAssignTMobile implements OperatorAssign{

    private WebDriver driver = new HtmlUnitDriver();

    @Override
    public Operator getOperator(String number, SiteConnector connector){

        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(java.util.logging.Level.OFF);
        java.util.logging.Logger.getLogger("org.apache.http").setLevel(java.util.logging.Level.OFF);

        if(number.length() != 9) {
            return Operator.Unknown;
        }
        try{
            driver = connector.getPage(driver, "http://download.t-mobile.pl/updir/");
            WebElement number_form = driver.findElement(By.id("msisdn"));
            WebElement button = driver.findElement(By.id("button1"));
            number_form.sendKeys(number);
            driver = connector.getFormResult(driver, button);
            String operator = driver.findElement(By.xpath("/html/body/form/table/tbody/tr[3]/td[2]/table[2]/tbody/tr[3]/td[2]")).getText();
            return convertStringToOperator(operator);
        }
        catch(NoSuchElementException|java.lang.IllegalStateException e){
            return Operator.Unknown;
        }
    }

    @Override
    public Operator convertStringToOperator(String operator){
        switch(operator){
            case "Heyah": return Operator.TMobile;
            case "PTC": return Operator.TMobile;
            case "PTC / T-Mobile": return Operator.TMobile;
            case "Plus" : return Operator.Plus;
            case "Plus / Simplus / 36.6 / mBank / Mova / FmMobile / FreeM": return Operator.Plus;
            case "Orange": return Operator.Orange;
            case "P4": return Operator.Play;
            default: System.out.println(operator); return Operator.Unknown;
        }
    }

}
