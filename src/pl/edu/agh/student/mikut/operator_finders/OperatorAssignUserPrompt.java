package pl.edu.agh.student.mikut.operator_finders;

import pl.edu.agh.student.mikut.billing_extender.Operator;

import javax.swing.*;

/**
 * Created by pawnow on 2014-11-29.
 */
public class OperatorAssignUserPrompt implements OperatorAssign {
    public Operator getOperator(String phoneNumber, SiteConnector connector){
        Object[] possibilities = {"TMobile", "Plus", "Orange", "Play", "Nieznany"};
        String operatorName = (String)JOptionPane.showInputDialog(
                null,
                "Podaj operatora dla numeru: " + phoneNumber,
                "Prosba o podanie operatora",
                JOptionPane.PLAIN_MESSAGE,
                null,
                possibilities,
                "Nieznany");
        if ((operatorName != null) && (operatorName.length() > 0)) {
            return convertStringToOperator(operatorName);
        }
        return Operator.Unknown;
    }

    @Override
    public Operator convertStringToOperator(String operator){
        try{
            Operator op = Operator.valueOf(operator);
            return op;
        }
        catch(IllegalArgumentException e){
            return Operator.Unknown;
        }
    }
}
