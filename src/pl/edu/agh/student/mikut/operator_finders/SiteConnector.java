package pl.edu.agh.student.mikut.operator_finders;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by pawnow on 2015-01-16.
 */
public class SiteConnector {

    public WebDriver getPage(WebDriver driver, String url) {
        driver.get(url);
        return driver;
    }

    public WebDriver getFormResult(WebDriver driver, WebElement button) {
        button.click();
        return driver;
    }
}
