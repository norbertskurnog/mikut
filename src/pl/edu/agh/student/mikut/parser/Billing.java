package pl.edu.agh.student.mikut.parser;

import pl.edu.agh.student.mikut.billing_extender.BillingElement;

import java.util.*;

public class Billing {

	private Map<String, BillingElement> elements = new HashMap<String, BillingElement>();
	private Collection<BillingElement> elements2 = new LinkedList<BillingElement>();

	public void addBillingElement(LineDetails detail) {

		if (elements.containsKey(detail.getPhoneNumber())) {
			BillingElement billingElement = elements.get(detail.getPhoneNumber());
			billingElement.setPhoneNumber(detail.getPhoneNumber());
			billingElement.setCallTime(detail.getCallTime() + billingElement.getCallTime());
			billingElement.setSmsAmount(detail.getSmsAmount() + billingElement.getSmsAmount());

		} else {
			BillingElement element = new BillingElement(detail.getPhoneNumber(), detail.getCallTime(), detail.getSmsAmount());
			elements.put(detail.getPhoneNumber(), element);
			elements2.add(new BillingElement(detail.getPhoneNumber(), 1, 1));

		}
	}

	public Collection<BillingElement> returnValue(){
		for(BillingElement x : elements2){
			String number;
			number = x.getPhoneNumber();
			x.setCallTime(this.getCallTime(x.getPhoneNumber()));
			x.setSmsAmount(this.getSmsAmountForNumber(x.getPhoneNumber()));
			x.setPhoneNumber(x.getPhoneNumber());
			if (!Objects.equals(number, "Internet") && number.length() == 11){
				x.setPhoneNumber(number.substring(2, 11));
			}
		}
		return elements2;
	}

	public Map<String, BillingElement> getBilling(){
		return elements;
	}


	public boolean containsNumber(String s) {
		return elements.containsKey(s);
	}

	public int getSmsAmountForNumber(String s) {
		BillingElement billingElement = elements.get(s);
		if (billingElement != null) {
			return billingElement.getSmsAmount();
		}
		return 0;
	}

	public int getCallTime(String s) {
		BillingElement billingElement = elements.get(s);
		if (billingElement != null) {
			return billingElement.getCallTime();
		}
		return 0;
	}
}