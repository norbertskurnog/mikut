package pl.edu.agh.student.mikut.parser;

import pl.edu.agh.student.mikut.comparator.OfferComparator;

import java.io.File;
import java.io.IOException;

/**
 * Created by Three on 2014-11-30.
 */
public class GUI {
    private Parser parser;
    private OfferComparator offerComparator;

    public GUI (OfferComparator offerComparator){
        this.offerComparator = offerComparator;
    }

    // dla kompatybilnosci, bo nie wiem co sie dzieje w całym kodzie
    public GUI(){
    }

    public void run (File billingFile){
        String filePath = billingFile.getPath();
        ParserController parserController = ParserController.getInstance(offerComparator);

        /* TODO: ZBEDNE. Ten exception jest sprawdzany w momencie otwarcia pliku, a tutaj plik jest otwierany ponownie ;/
         * ogarnijcie to, zamiast String mozecie przyjac File, obiecuje ze bedzie poprawny ;d */
        try{
            parserController.run(filePath);
        } catch (IOException e){
            e.printStackTrace();
        }
    }

//    public static void main (String[] args) throws IOException {
//        String filePath = args[0];
//        ParserController parserController = ParserController.getInstance();
//        parserController.run(filePath);
//    }
}
