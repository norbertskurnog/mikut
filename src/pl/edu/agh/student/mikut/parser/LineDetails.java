package pl.edu.agh.student.mikut.parser;

/**
 * Created by Three on 2014-11-30.
 */
public class LineDetails {

    private String phoneNumber;
    private int callTime;
    private int smsAmount;

    public LineDetails(String phoneNumber, int callTime, int smsAmount){
        this.phoneNumber = phoneNumber;
        this.callTime = callTime;
        this.smsAmount = smsAmount;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getCallTime() {
        return callTime;
    }

    public void setCallTime(int callTime) {
        this.callTime = callTime;
    }

    public int getSmsAmount() {
        return smsAmount;
    }

    public void setSmsAmount(int smsAmount) {
        this.smsAmount = smsAmount;
    }
}
