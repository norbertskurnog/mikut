package pl.edu.agh.student.mikut.parser;

import java.util.Arrays;
import java.util.Collection;

import com.snowtide.pdf.OutputTarget;
import com.snowtide.pdf.PDFTextStream;

public class PDFParser implements Parser {

	private Billing billing;

	private Collection<String> billingLines;

    public String parseToString(String pdfFilePath) {
		PDFTextStream pdfts = new PDFTextStream(pdfFilePath);
		StringBuilder text = new StringBuilder();
		pdfts.pipe(new OutputTarget(text));
		pdfts.close();
		return text.toString();
	}

    public void setSource(Billing file) {
	}

    private void invokeModule2(Billing billing) {
	}

    private LineDetails findDetails(String line) {
		LineDetails detail = new LineDetails("0",0,0);

		//szukamy nr, typu polaczenia i wartosci (Tmobile) i setujemy w detail
		return detail;
	}
    
    private Collection<String> findLines(String text) {
		return Arrays.asList(text.split("\\r?\\n"));
	}

	public Billing parse(String filePath) {
		String resultString = parseToString(filePath);
		Billing billing = new Billing();
		Collection<String> lines = findLines(resultString);
		for (String line : lines){
			LineDetails detail = findDetails(line);
			billing.addBillingElement(detail);
		}

		return billing;
	}
}