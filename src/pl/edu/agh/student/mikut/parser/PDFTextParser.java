package pl.edu.agh.student.mikut.parser;

import java.io.IOException;
 
import com.snowtide.pdf.OutputTarget;
import com.snowtide.pdf.PDFTextStream;
 
public class PDFTextParser {
    public static void main (String[] args) throws IOException {
        String pdfFilePath = args[0];
        PDFTextStream pdfts = new PDFTextStream(pdfFilePath); 
        StringBuilder text = new StringBuilder(1024);
        pdfts.pipe(new OutputTarget(text));
        pdfts.close();
        System.out.printf("The text extracted from %s is:", pdfFilePath);
        System.out.println(text);
    }
}