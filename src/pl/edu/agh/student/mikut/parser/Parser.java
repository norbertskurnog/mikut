package pl.edu.agh.student.mikut.parser;

import java.io.IOException;

public interface Parser {

    public Billing parse(String filePath) throws IOException;

    
}