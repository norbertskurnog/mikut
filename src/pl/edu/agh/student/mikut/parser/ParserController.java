package pl.edu.agh.student.mikut.parser;

import pl.edu.agh.student.mikut.billing_extender.OperatorFinder;
import pl.edu.agh.student.mikut.comparator.OfferComparator;
import pl.edu.agh.student.mikut.operator_finders.OperatorAssign;
import pl.edu.agh.student.mikut.operator_finders.OperatorAssignSMSPriv;
import pl.edu.agh.student.mikut.operator_finders.SiteConnector;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

// TODO: pousuwac zdublowane metody

public class ParserController {
	private Parser parser;
	private OperatorFinder operatorFinder = new OperatorFinder();
    private OfferComparator offerComparator;

	private ParserController(OfferComparator offerComparator) {
        this.offerComparator = offerComparator;
    }

    private ParserController() {
    }

	private static ParserController instance = null;

	public static ParserController getInstance(OfferComparator offerComparator)
	{
		if(instance == null) {
			instance = new ParserController(offerComparator);
		}
		return instance;
	}

    public static ParserController getInstance()
    {
        if(instance == null) {
            instance = new ParserController();
        }
        return instance;
    }


    public void runRequest(Parser parser)
	{
	}

	public void run(String filePath) throws IOException{
		Parser test = new PlayParser();
		Billing result = ParserFactory.getInstance().build(filePath).parse(filePath);
		List<OperatorAssign> source = new LinkedList<>();
		//has to be changed later
		source.add(new OperatorAssignSMSPriv());
		SiteConnector connector = new SiteConnector();
		operatorFinder.setBillingOperator(offerComparator, result.returnValue(), source);
	}
}