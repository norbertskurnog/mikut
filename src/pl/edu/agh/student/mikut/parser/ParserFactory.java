package pl.edu.agh.student.mikut.parser;

/**
 * Created by Gielo on 2015-01-26.
 */
public class ParserFactory {
    private ParserFactory() {
    }

    private static ParserFactory instance;

    public static synchronized ParserFactory getInstance() {
        if (instance == null) {
            instance = new ParserFactory();
        }
        return instance;
    }

    public Parser build(String filePath) {
        String extension = filePath.substring(filePath.length() - 3, filePath.length()).toLowerCase();
        switch (extension) {
            case "pdf":
                return new TMobileParser();
            case "csv":
                return new PlayParser();
            default:
                throw new IllegalStateException("invalid extension");
        }
    }
}
