package pl.edu.agh.student.mikut.parser;

import au.com.bytecode.opencsv.CSVReader;
import com.google.common.base.Optional;
import com.google.common.base.Preconditions;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;

/**
 * Created by Three on 2014-11-30.
 */
public class PlayParser implements Parser {

    private List<String[]> parseToList(String filePath) throws IOException{
       // CSVReader reader = new CSVReader(new FileReader("yourfile.csv"));
        CSVReader reader = new CSVReader(new FileReader(filePath));

        return reader.readAll();
    }

    private Optional<LineDetails> findDetails(String[] collumns) {
        Preconditions.checkArgument(collumns.length >= 9);

        if (collumns[1].contains("Wych")) {
            LineDetails detail = new LineDetails(collumns[5], 0, 0);
            if (collumns[2].equals("SMS")) {
                detail.setSmsAmount(1);
            }
            else if (collumns[2].contains("Rozmowy")) {
                String[] tokens = collumns[7].split(":");
                int minutes = Integer.parseInt(tokens[0]);
                int seconds = Integer.parseInt(tokens[1].substring(0, 2));
                int duration = 60 * minutes + seconds;
                detail.setCallTime(duration);
            } else if (collumns[2].contains("Dane")) {
                String kilobytes = collumns[7].substring(0, collumns[7].length() - 3);
                detail.setCallTime(Integer.valueOf(kilobytes));
            }
            return Optional.fromNullable(detail);
        }
        return Optional.absent();
    }

    public Billing parse(String filePath) throws IOException{
        List<String[]> resultList = parseToList(filePath);
        Billing billing = new Billing();
        for (String[] collumns : resultList) {
            Optional<LineDetails> detail = findDetails(collumns);
            if (detail.isPresent()) {
                billing.addBillingElement(detail.get());
            }
        }

        return billing;
    }
}
