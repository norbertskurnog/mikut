package pl.edu.agh.student.mikut.parser;
import com.google.common.base.Optional;
import com.snowtide.PDF;
import com.snowtide.pdf.OutputTarget;
import com.snowtide.pdf.PDFTextStream;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Three on 2015-01-19.
 */
public class TMobileParser implements Parser {

    public static final Pattern SMS_PATTERN = Pattern.compile("SMS[^0-9]+[0-9]+");
    public static final Pattern CALL_PATTERN = Pattern.compile("[0-9]+[ ]+[0-9]+[ ]+[0-9]+");
    public static final Pattern NET_PATTERN = Pattern.compile("internet[^1-9]+[0-9]+[^kB]");

    public String parseToString(String pdfFilePath) {
        PDFTextStream pdfts = new PDFTextStream(pdfFilePath);
        StringBuilder text = new StringBuilder();
        pdfts.pipe(new OutputTarget(text));
        pdfts.close();
        return text.toString();
    }

    private Optional<LineDetails> findDetails(String line) {
        LineDetails details = null;
        if (SMS_PATTERN.matcher(line).find()) {
            String[] data = extractDataFromLine(SMS_PATTERN, line);
            String number = data[data.length - 1];
            details = new LineDetails(number, 0, 1);
        } else if (CALL_PATTERN.matcher(line).find()) {
            String[] data = extractDataFromLine(CALL_PATTERN, line);
            String seconds = data[data.length - 1];
            String number = data[0];
            details = new LineDetails(number, Integer.valueOf(seconds), 0);
        } else if (NET_PATTERN.matcher(line).find()) {
            String[] data = extractDataFromLine(NET_PATTERN, line);
            String amount = data[data.length - 1];
            details = new LineDetails("internet", Integer.valueOf(amount), 0);
        }

        //szukamy nr, typu polaczenia i wartosci (Tmobile) i setujemy w detail
        return Optional.fromNullable(details);
    }

    private String[] extractDataFromLine(Pattern pattern, String line) {
        Matcher matcher = pattern.matcher(line);
        matcher.find();
        String substring = line.substring(matcher.start(), matcher.end());
        return substring.split(" ");
    }

    private Collection<String> findLines(String text) {
        return Arrays.asList(text.split("\\r?\\n"));
    }

    @Override
    public Billing parse(String filePath) throws IOException {
        String resultString = parseToString(filePath);
        Billing billing = new Billing();
        Collection<String> lines = findLines(resultString);
        for (String line : lines){
            Optional<LineDetails> details = findDetails(line);
            if (details.isPresent()) {
                billing.addBillingElement(details.get());
            }
        }

        return billing;

    }
}