package pl.edu.agh.student.mikut.test;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import pl.edu.agh.student.mikut.parser.Billing;
import pl.edu.agh.student.mikut.parser.PlayParser;

import java.io.IOException;

import static org.fest.assertions.Assertions.assertThat;

public class CSVParserTest {

    private PlayParser instance;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private static final String FILEPATH = "billing.csv";
    private static final String PHONE_NUMBER = "48721827317";


    @Before
    public void setUp() throws Exception {
        instance = new PlayParser();
    }

    @Test
    public void shouldParserThrowExceptionFileDoesNotExist() throws Exception {
        //given
        String filePath = "filenotexisting.csv";        //make sure that file does not exists physically
        //when
        expectedException.expect(IOException.class);
        instance.parse(filePath);
        //then
        //expect IOException
    }

    @Test
    public void shouldParserThrowExceptionIfGivenFileIsNotCSV() throws Exception {
        //given
        String filepath = "pdffile.pdf";
        //when
        expectedException.expect(IOException.class);
        instance.parse(filepath);
        //then
        //expect Exception
    }

    @Test
    public void shouldProperlyParsedBillingContainProperPhoneNumber() throws Exception {
        //given
        //when
        Billing billing = instance.parse(FILEPATH);
        //then
        assertThat(billing.containsNumber(PHONE_NUMBER)).isEqualTo(true);
    }

    @Test
    public void shouldProperlyParsedBillingContainProperSmsAmountForNumber() throws Exception {
        //given
        //when
        Billing billing = instance.parse(FILEPATH);
        //then
        assertThat(billing.getSmsAmountForNumber(PHONE_NUMBER)).isEqualTo(8);
    }

    @Test
    public void shouldProperlyParsedBillingContainProperInternetAmount() throws Exception {
        //given

        //when
        Billing billing = instance.parse(FILEPATH);
        //then
        assertThat(billing.getCallTime("Internet")).isEqualTo(53797);
    }

    @Test
    public void shouldProperlyParsedBillingContainProperCalltimeForNumber() throws Exception {
        //given

        //when
        Billing billing = instance.parse(FILEPATH);
        //then
        assertThat(billing.getCallTime(PHONE_NUMBER)).isEqualTo(22);
    }
}
