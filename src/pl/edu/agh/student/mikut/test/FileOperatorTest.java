package pl.edu.agh.student.mikut.test;

import junit.framework.TestCase;
import org.junit.Test;
import pl.edu.agh.student.mikut.billing_extender.BillingElementExtended;
import pl.edu.agh.student.mikut.billing_extender.FileOperator;
import pl.edu.agh.student.mikut.billing_extender.Operator;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by pawnow on 2015-01-16.
 */
public class FileOperatorTest extends TestCase {

    protected FileOperator op;

    public void setUp(){
        op = new FileOperator();
    }

    @Test
    public void testSaveFile(){
        List<BillingElementExtended> billing = new LinkedList<>();
        billing.add(new BillingElementExtended("111222333",0,0, Operator.Play));
        billing.add(new BillingElementExtended("100000000",0,0, Operator.Orange));
        billing.add(new BillingElementExtended("100000001",0,0, Operator.TMobile));
        billing.add(new BillingElementExtended("100000002",0,0, Operator.Plus));
        billing.add(new BillingElementExtended("123456789",0,0, Operator.Unknown));
        assertEquals(op.saveFile(billing), true);
        HashMap<String, Operator> map = (HashMap<String, Operator>) op.readFile();
        assertEquals(map.containsKey("111222333"),true);
        assertEquals(map.containsKey("100000000"),true);
        assertEquals(map.containsKey("100000001"),true);
        assertEquals(map.containsKey("100000002"),true);
        assertEquals(map.containsKey("123456789"),false);
        assertEquals(map.get("111222333"),Operator.Play);
        assertEquals(map.get("100000000"),Operator.Orange);
        assertEquals(map.get("100000001"),Operator.TMobile);
        assertEquals(map.get("100000002"),Operator.Plus);

    }

}
