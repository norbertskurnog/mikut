package pl.edu.agh.student.mikut.test;

import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import pl.edu.agh.student.mikut.billing_extender.BillingElement;
import pl.edu.agh.student.mikut.billing_extender.BillingElementExtended;
import pl.edu.agh.student.mikut.billing_extender.Operator;
import pl.edu.agh.student.mikut.billing_extender.OperatorFinder;
import pl.edu.agh.student.mikut.comparator.OfferComparator;
import pl.edu.agh.student.mikut.operator_finders.OperatorAssign;
import pl.edu.agh.student.mikut.operator_finders.OperatorAssignSMSPriv;
import pl.edu.agh.student.mikut.operator_finders.SiteConnector;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.verifyNew;


/**
 * Created by pawnow on 2014-12-07.
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest(OperatorFinder.class)
public class InvokeOfferComparatorTest extends TestCase {
    /* Test checks if OfferComparator is set up when we call setBillingOperator on OperatorFinder
     */
    @Test
    public void testInvokeOfferComparison() throws Exception {

        List<OperatorAssign> testSource = new LinkedList<>();
        List <OperatorAssign> spySource = spy(testSource);

        Collection<BillingElement> testBilling = new LinkedList<>();
        testBilling.add(new BillingElement("111222333",0,0));//Play
        testBilling.add(new BillingElement("100000000",0,0));//Orange
        testBilling.add(new BillingElement("100000001",0,0));//TMobile
        testBilling.add(new BillingElement("100000002",0,0));//Plus
        testBilling.add(new BillingElement("123456789",0,0));//Unknown
        SiteConnector connector = new SiteConnector();
        OperatorAssignSMSPriv smsPrivMock = Mockito.mock(OperatorAssignSMSPriv.class);
        when(smsPrivMock.getOperator("111222333",connector)).thenReturn(Operator.Play);
        when(smsPrivMock.getOperator("100000000",connector)).thenReturn(Operator.Orange);
        when(smsPrivMock.getOperator("100000001",connector)).thenReturn(Operator.TMobile);
        when(smsPrivMock.getOperator("100000002",connector)).thenReturn(Operator.Plus);
        when(smsPrivMock.getOperator("123456789",connector)).thenReturn(Operator.Unknown);
        doReturn(smsPrivMock).when(spySource).get(0);
        when(spySource.size()).thenReturn(1);

        OperatorFinder finder = new OperatorFinder();

        OfferComparator oc = new OfferComparator();
        PowerMockito.whenNew(OfferComparator.class).withNoArguments().thenReturn(oc);
        PowerMockito.whenNew(SiteConnector.class).withNoArguments().thenReturn(connector);

        finder.setBillingOperator(testBilling, spySource);

        try{
            verifyNew(OfferComparator.class).withNoArguments();
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertEquals(oc.billingsData.size(), 5);
        for (BillingElementExtended t : oc.billingsData) {
            if (t.getPhoneNumber().equals("111222333"))
                assertEquals(t.getOperatorName(), Operator.Play);
            if (t.getPhoneNumber().equals("100000000"))
                assertEquals(t.getOperatorName(), Operator.Orange);
            if (t.getPhoneNumber().equals("100000001"))
                assertEquals(t.getOperatorName(), Operator.TMobile);
            if (t.getPhoneNumber().equals("100000002"))
                assertEquals(t.getOperatorName(), Operator.Plus);
            if (t.getPhoneNumber().equals("123456789"))
                assertEquals(t.getOperatorName(), Operator.Unknown);
        }
    }

}

