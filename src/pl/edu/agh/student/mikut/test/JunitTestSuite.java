package pl.edu.agh.student.mikut.test;

/**
 * Created by pawnow on 2014-11-29.
 */
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
@RunWith(Suite.class)
@Suite.SuiteClasses({
        OperatorAssignSMSPrivTest.class,
        OperatorTMobileTest.class,
        SMSPrivSiteConnectorTest.class,
        TMobileSiteConnectorTest.class,
        OperatorAssignUserPromptTest.class,
        ProgressBarTest.class,
        //OperatorFinderTest.class,
        //InvokeOfferComparatorTest.class
})
public class JunitTestSuite {

}
