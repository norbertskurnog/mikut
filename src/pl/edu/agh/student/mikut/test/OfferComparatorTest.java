//package pl.edu.agh.student.mikut.test;
//
//import pl.edu.agh.student.mikut.billing_extender.Operator;
//import pl.edu.agh.student.mikut.comparator.Offer;
//import pl.edu.agh.student.mikut.comparator.OfferComparator;
//import pl.edu.agh.student.mikut.comparator.OfferService;
//import pl.edu.agh.student.mikut.comparator.ServiceType;
//import pl.edu.agh.student.mikut.comparator.DBConnector;
//import org.junit.Assert;
//import org.junit.Test;
//
//import java.util.LinkedList;
//import java.util.List;
//
//public class OfferComparatorTest {
//
//    @Test
//    public void testCompareOffers(){
//        List<OfferService> osList = new LinkedList<OfferService>();
//
//        Offer o = new Offer("xDD", Operator.Play, osList);
//
//        OfferService os1 = new OfferService(ServiceType.Call, 60, 1.39, o);
//        OfferService os2 = new OfferService(ServiceType.SMS, 60, 1.39, o);
//        OfferService os3 = new OfferService(ServiceType.SMS_pack, 33, 1339, o);
//        OfferService os4 = new OfferService(ServiceType.Internet, 60, 1.39, o);
//
//        osList.add(os1);
//        osList.add(os2);
//        osList.add(os3);
//        osList.add(os4);
//
//        DBConnector connection = new DBConnector();
//        connection.saveOfferInDB(o);
//
//        Offer o2 = new Offer("xDD", Operator.Play, osList);
//
//        OfferService os11 = new OfferService(ServiceType.Call, 33, 1.3933, o2);
//        OfferService os21 = new OfferService(ServiceType.SMS, 60, 1.319, o2);
//        OfferService os31 = new OfferService(ServiceType.SMS_pack, 3323, 13239, o2);
//        OfferService os41 = new OfferService(ServiceType.Internet, 630, 1.339, o2);
//
//        osList.add(os1);
//        osList.add(os2);
//        osList.add(os3);
//        osList.add(os4);
//
//        DBConnector connection2 = new DBConnector();
//        connection2.saveOfferInDB(o);
//        connection2.saveOfferInDB(o2);
//
//        OfferComparator offerComparator = new OfferComparator();
//        List<Offer> comparedOffers = offerComparator.compareOffers();
//
//        List<Offer> expectedResult = new LinkedList<Offer>();
//        expectedResult.add(o);
//        expectedResult.add(o2);
//
//        Assert.assertEquals(comparedOffers, expectedResult);
//    }
//}
