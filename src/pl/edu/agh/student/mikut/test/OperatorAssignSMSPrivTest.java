package pl.edu.agh.student.mikut.test;

import junit.framework.TestCase;
import org.junit.Test;
import pl.edu.agh.student.mikut.billing_extender.Operator;
import pl.edu.agh.student.mikut.operator_finders.OperatorAssignSMSPriv;
import pl.edu.agh.student.mikut.operator_finders.SiteConnector;

public class OperatorAssignSMSPrivTest extends TestCase {

    protected OperatorAssignSMSPriv operatorAssigner;

    protected void setUp(){
        operatorAssigner = new OperatorAssignSMSPriv();
    }

    @Test
    public void testGetPlay(){
        Operator play = operatorAssigner.getOperator("603772702", new SiteConnector());
        assertEquals(Operator.Play, play);
    }

    @Test
    public void testGetPlus() {
        Operator plus = operatorAssigner.getOperator("605384923", new SiteConnector());
        assertEquals(Operator.Plus, plus);
    }

    @Test
    public void testGetOrange() {
        Operator orange = operatorAssigner.getOperator("501847954", new SiteConnector());
        assertEquals(Operator.Orange, orange);
    }

    @Test
    public void testGetTmobile() {
        Operator tmobile = operatorAssigner.getOperator("538847392", new SiteConnector());
        Operator tmobile2 = operatorAssigner.getOperator("603772703", new SiteConnector());
        assertEquals(Operator.TMobile, tmobile);
        assertEquals(Operator.TMobile, tmobile2);
    }

    @Test
    public void testGetUnknown() {
        SiteConnector connector = new SiteConnector();
        Operator unknown1 = operatorAssigner.getOperator("100000000",connector);
        Operator unknown2 = operatorAssigner.getOperator("1",connector);
        Operator unknown3 = operatorAssigner.getOperator("999999999",connector);
        assertEquals(Operator.Unknown, unknown1);
        assertEquals(Operator.Unknown, unknown2);
        assertEquals(Operator.Unknown, unknown3);
    }

}