package pl.edu.agh.student.mikut.test;

import junit.framework.TestCase;
import org.junit.Test;
import pl.edu.agh.student.mikut.billing_extender.Operator;
import pl.edu.agh.student.mikut.operator_finders.OperatorAssignUserPrompt;

/**
 * Created by pawnow on 2014-12-06.
 */
public class OperatorAssignUserPromptTest  extends TestCase {

    protected OperatorAssignUserPrompt operatorAssigner;

    protected void setUp(){
        operatorAssigner = new OperatorAssignUserPrompt();
    }

    @Test
    public void testConvertStringToOperator(){
        assertEquals(operatorAssigner.convertStringToOperator("Play"), Operator.Play);
        assertEquals(operatorAssigner.convertStringToOperator("Plus"), Operator.Plus);
        assertEquals(operatorAssigner.convertStringToOperator("Orange"), Operator.Orange);
        assertEquals(operatorAssigner.convertStringToOperator("TMobile"), Operator.TMobile);
        assertEquals(operatorAssigner.convertStringToOperator("Nieznany"), Operator.Unknown);
        assertEquals(operatorAssigner.convertStringToOperator("Hfym"), Operator.Unknown);
    }

}
