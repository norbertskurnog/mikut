package pl.edu.agh.student.mikut.test;

import junit.framework.TestCase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import pl.edu.agh.student.mikut.billing_extender.BillingElement;
import pl.edu.agh.student.mikut.billing_extender.BillingElementExtended;
import pl.edu.agh.student.mikut.billing_extender.Operator;
import pl.edu.agh.student.mikut.operator_finders.OperatorAssign;
import pl.edu.agh.student.mikut.operator_finders.OperatorAssignSMSPriv;
import pl.edu.agh.student.mikut.billing_extender.OperatorFinder;
import pl.edu.agh.student.mikut.operator_finders.SiteConnector;

import static org.mockito.Mockito.*;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class OperatorFinderTest extends TestCase {

    @Test
    public void testFindOperators(){

        /* BillingElement mock not used - too simple class to test, only getters and setters
        List <BillingElement> testBilling = new LinkedList<BillingElement>();
        List <BillingElement> spyBilling = spy(testBilling);

        BillingElement num1 = Mockito.mock(BillingElement.class);
        when(num1.getPhoneNumber()).thenReturn("111222333");
        doReturn(num1).when(spyBilling).get(0);

        BillingElement num2 = Mockito.mock(BillingElement.class);
        when(num1.getPhoneNumber()).thenReturn("100000000");
        doReturn(num2).when(spyBilling).get(1);

        BillingElement num3 = Mockito.mock(BillingElement.class);
        when(num1.getPhoneNumber()).thenReturn("123456789");
        doReturn(num3).when(spyBilling).get(2);
        when(spyBilling.size()).thenReturn(3);*/
        File cacheFile = new File("operatorCache.txt");
        if(cacheFile.exists()){
           cacheFile.delete();
        }
        List <OperatorAssign> testSource = new LinkedList<>();
        List <OperatorAssign> spySource = spy(testSource);

        Collection<BillingElement> testBilling = new LinkedList<>();
        testBilling.add(new BillingElement("111222333",0,0));//Play
        testBilling.add(new BillingElement("100000000",0,0));//Orange
        testBilling.add(new BillingElement("100000001",0,0));//TMobile
        testBilling.add(new BillingElement("100000002",0,0));//Plus
        testBilling.add(new BillingElement("123456789",0,0));//Unknown

        SiteConnector connector = new SiteConnector();

        OperatorAssignSMSPriv smsPrivMock = Mockito.mock(OperatorAssignSMSPriv.class);

        when(smsPrivMock.getOperator("111222333",connector)).thenReturn(Operator.Play);
        when(smsPrivMock.getOperator("100000000",connector)).thenReturn(Operator.Orange);
        when(smsPrivMock.getOperator("100000001",connector)).thenReturn(Operator.TMobile);
        when(smsPrivMock.getOperator("100000002",connector)).thenReturn(Operator.Plus);
        when(smsPrivMock.getOperator("123456789",connector)).thenReturn(Operator.Unknown);

        doReturn(smsPrivMock).when(spySource).get(0);
        when(spySource.size()).thenReturn(1);

        OperatorFinder testFinder = new OperatorFinder();
        Method method = null;
        try {
            method = (OperatorFinder.class).getDeclaredMethod("findOperators", Collection.class, List.class, SiteConnector.class );
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }

        method.setAccessible(true);
        Collection<BillingElementExtended> testCollection = null;

        try {
            testCollection = (Collection<BillingElementExtended>) method.invoke(testFinder, testBilling, spySource, connector);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        for (BillingElementExtended t : testCollection) {
            if (t.getPhoneNumber().equals("111222333"))
                assertEquals(t.getOperatorName(), Operator.Play);
            if (t.getPhoneNumber().equals("100000000"))
                assertEquals(t.getOperatorName(), Operator.Orange);
            if (t.getPhoneNumber().equals("100000001"))
                assertEquals(t.getOperatorName(), Operator.TMobile);
            if (t.getPhoneNumber().equals("100000002"))
                assertEquals(t.getOperatorName(), Operator.Plus);
            if (t.getPhoneNumber().equals("123456789"))
                assertEquals(t.getOperatorName(), Operator.Unknown);
        }

        verify(smsPrivMock, times(1)).getOperator("111222333",connector);
        verify(smsPrivMock, times(1)).getOperator("100000000",connector);
        verify(smsPrivMock, times(1)).getOperator("100000001",connector);
        verify(smsPrivMock, times(1)).getOperator("100000002",connector);
        verify(smsPrivMock, times(1)).getOperator("123456789",connector);
        assertEquals(testCollection.size(), 5);

    }
}