package pl.edu.agh.student.mikut.test;

import junit.framework.TestCase;
import org.junit.Test;
import pl.edu.agh.student.mikut.billing_extender.BillingElement;
import pl.edu.agh.student.mikut.billing_extender.OperatorFinder;
import pl.edu.agh.student.mikut.operator_finders.OperatorAssign;
import pl.edu.agh.student.mikut.operator_finders.OperatorAssignSMSPriv;
import pl.edu.agh.student.mikut.operator_finders.SiteConnector;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by pawnow on 2015-01-17.
 */
public class ProgressBarTest extends TestCase  {

    @Test
    public void testProgressBar(){
        OperatorFinder op = new OperatorFinder();
        Collection<BillingElement> testBilling = new LinkedList<>();
        testBilling.add(new BillingElement("603772702",0,0));//Play
        testBilling.add(new BillingElement("501847954",0,0));//Orange
        testBilling.add(new BillingElement("538847392",0,0));//TMobile
        testBilling.add(new BillingElement("605384923",0,0));//Plus
        testBilling.add(new BillingElement("123456789",0,0));//Unknown
        List<OperatorAssign> testSource = new LinkedList<>();
        testSource.add(new OperatorAssignSMSPriv());
        Method method = null;
        try {
            method = OperatorFinder.class.getDeclaredMethod("findOperators", Collection.class,
                    List.class, SiteConnector.class);
            method.setAccessible(true);
            method.invoke(op, testBilling, testSource, new SiteConnector());
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        assertEquals(true, true);
    }
}
