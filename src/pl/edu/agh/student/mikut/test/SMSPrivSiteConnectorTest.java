package pl.edu.agh.student.mikut.test;

import junit.framework.TestCase;
import org.junit.Test;
import org.mockito.Mockito;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import pl.edu.agh.student.mikut.billing_extender.Operator;
import pl.edu.agh.student.mikut.operator_finders.OperatorAssignSMSPriv;
import pl.edu.agh.student.mikut.operator_finders.SiteConnector;

import java.io.IOException;
import java.lang.reflect.Field;

/**
 * Created by pawnow on 2014-12-14.
 */
public class SMSPrivSiteConnectorTest extends TestCase {

    protected OperatorAssignSMSPriv operatorAssigner;

    protected void setUp(){
        operatorAssigner = new OperatorAssignSMSPriv();
    }

    @Test
    public void testGetPlayConnectorMock() throws IOException {

        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(java.util.logging.Level.OFF);
        java.util.logging.Logger.getLogger("org.apache.http").setLevel(java.util.logging.Level.OFF);

        WebDriver driver = new HtmlUnitDriver();
        WebDriver driver2 = new HtmlUnitDriver();
        SiteConnector connector = Mockito.mock(SiteConnector.class);

        Field webDriverField;
        try {
            webDriverField = OperatorAssignSMSPriv.class.getDeclaredField("driver");
            webDriverField.setAccessible(true);
            webDriverField.set(operatorAssigner, driver);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        driver.get("file:page.html");
        WebElement button = driver.findElement(By.xpath("/html/body/table/tbody/tr[7]/td[1]/form/input[2]"));
        Mockito.doReturn(driver).when(connector).getPage(driver, "http://sms.priv.pl/analiza-numerow/");
        driver2.get("file:playPage.html");
        Mockito.doReturn(driver2).when(connector).getFormResult(driver, button);
        Operator play = operatorAssigner.getOperator("603772702", connector);
        assertEquals(Operator.Play, play);

    }

}
