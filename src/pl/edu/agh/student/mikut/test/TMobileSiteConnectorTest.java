package pl.edu.agh.student.mikut.test;

import junit.framework.TestCase;
import org.junit.Test;
import org.mockito.Mockito;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import pl.edu.agh.student.mikut.billing_extender.Operator;
import pl.edu.agh.student.mikut.operator_finders.OperatorAssignTMobile;
import pl.edu.agh.student.mikut.operator_finders.SiteConnector;

import java.io.IOException;
import java.lang.reflect.Field;

import static org.junit.Assert.assertEquals;

/**
 * Created by pawnow on 2015-01-16.
 */
public class TMobileSiteConnectorTest  extends TestCase {
    protected OperatorAssignTMobile operatorAssigner;

    protected void setUp(){
        operatorAssigner = new OperatorAssignTMobile();
    }

    @Test
    public void testGetPlayConnectorMock() throws IOException {

        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(java.util.logging.Level.OFF);
        java.util.logging.Logger.getLogger("org.apache.http").setLevel(java.util.logging.Level.OFF);

        WebDriver driver = new HtmlUnitDriver();
        WebDriver driver2 = new HtmlUnitDriver();
        SiteConnector connector = Mockito.mock(SiteConnector.class);

        Field webDriverField;
        try {
            webDriverField = OperatorAssignTMobile.class.getDeclaredField("driver");
            webDriverField.setAccessible(true);
            webDriverField.set(operatorAssigner, driver);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        driver.get("file:tpage.html");
        WebElement button = driver.findElement(By.id("button1"));
        Mockito.doReturn(driver).when(connector).getPage(driver, "http://download.t-mobile.pl/updir/");
        driver2.get("file:tplayPage.html");
        Mockito.doReturn(driver2).when(connector).getFormResult(driver, button);
        Operator play = operatorAssigner.getOperator("603772702", connector);
        assertEquals(Operator.Play, play);

    }
}
